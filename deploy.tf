
provider "azurerm" {
  version = "~>2.0"
  features {}
}

# Create virtual network
resource "azurerm_resource_group" "rg_solocal" {
  name = "DBA_SOL"
  location = "West Europe"


  tags = {
        ENV = "PROD"
    }
}

# Create virtual network
resource "azurerm_virtual_network" "network_solocal" {
    name                = "MyDBAVnet"
    address_space       = ["10.0.0.0/16"]
    location            = "West Europe"
    resource_group_name = azurerm_resource_group.rg_solocal.name

    tags = {
        ENV = "PROD"
    }
}

# Create subnet
resource "azurerm_subnet" "subnet_solocal" {
    name                 = "MyDBASubnet"
    resource_group_name  = azurerm_resource_group.rg_solocal.name
    virtual_network_name = azurerm_virtual_network.network_solocal.name
    address_prefixes       = ["10.0.1.0/24"]
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "nsg_solocal" {
    name                = "myNetworkSecurityGroup"
    location            = "West Europe"
    resource_group_name = azurerm_resource_group.rg_solocal.name

    security_rule {
        name                       = "SSH"
        priority                   = 300
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "VirtualNetwork"
    }

    tags = {
       ENV = "PROD"
    }
}


# Create network interface
resource "azurerm_network_interface" "nic_solocal" {
    name                      = "myNIC"
    location                  = "West Europe"
    resource_group_name       = azurerm_resource_group.rg_solocal.name

    ip_configuration {
        name                          = "myNicConfiguration"
        subnet_id                     = azurerm_subnet.subnet_solocal.id
        private_ip_address_allocation = "Dynamic"
        
    }

   tags = {
       ENV = "PROD"
    }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "example" {
    network_interface_id      = azurerm_network_interface.nic_solocal.id
    network_security_group_id = azurerm_network_security_group.nsg_solocal.id
}


# Create (and display) an SSH key
resource "tls_private_key" "solocal_ssh" {
  algorithm = "RSA"
  rsa_bits = 4096
}

 output "tls_private_key" { value = tls_private_key.solocal_ssh.private_key_pem }



# Create virtual machine
resource "azurerm_linux_virtual_machine" "solocal_vm" {
    name                  = "MyDBAVM"
    location              = "West Europe"
    resource_group_name   = azurerm_resource_group.rg_solocal.name
    network_interface_ids = [azurerm_network_interface.nic_solocal.id]
    size                  = "Standard_DS1_v2"

    os_disk {
        name              = "myOsDisk"
        caching           = "ReadWrite"
        storage_account_type = "Standard_LRS"   // Type of Disk 
    }

    source_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "18.04-LTS"
        version   = "latest"
    }

    computer_name  = "MyDBAVM"
    admin_username = "dba_solocal"
    disable_password_authentication = true

    admin_ssh_key {
        username       = "dba_solocal"
        public_key     = file("~/.ssh/id_rsa.pub")
       
    }

  

    tags = {
       ENV = "PROD"
    }

}

    resource "azurerm_managed_disk" "source" {
  name                 = "acctestmd1"
  location             = "West Europe"
  resource_group_name  = azurerm_resource_group.rg_solocal.name
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = "256"
  
  tags = {
    ENV = "PROD"
  }
}
